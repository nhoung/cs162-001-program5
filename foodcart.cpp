#include "foodcart.h"

//Molina Nhoung CS162
//Program 5
//The purpose of this program is to keep track of favorite foods from
//different food carts added by the user. Each information will have
//the name of the cart, favorite food from there, cost, decription
//of flavors, and a disliked food from the cart. The information
//will be displayed to the user after.

void welcome()
{
	cout << "\n***FAVORITE FOOD CART FOOD***"
		"\nThis program will keep track of your favorite food"
		"from different food carts. \nYou will be asked to enter:"
		"\n\t- Name of the food cart"
		"\n\t- Favorite food from there"
		"\n\t- Cost of the food"
		"\n\t- Description of flavors and what you life about the food"
		"\n\t- Food cart rating out of 10"
		"\nAfterwords, your list will be displayed" << endl;
}	

void read(node * & head)
{
	node * hold = head;
	head = new node;

	char temp[NAME];
	cout << "\nWhat's the name of the food cart?: ";
	cin.get(temp, NAME, '\n');
	cin.ignore(100, '\n');
	head->data.name = new char [strlen(temp) + 1];
	strcpy(head->data.name, temp);
	head->data.name[0] = toupper(head->data.name[0]);

	char temp1[SIZE];
	cout << "\nWhat's your favorite food from the cart?: ";
	cin.get(temp1, SIZE, '\n');
	cin.ignore(100, '\n');
	head->data.fave = new char [strlen(temp1) + 1];
	strcpy(head->data.fave, temp1);

	cout << "\nHow much does the food cost?: ";
	cin >> head->data.cost;
	cin.ignore(100, '\n');

	char temp2[SIZE];
	cout << "\nGive a description of flavors and what you like about the food: ";
	cin.get(temp2, SIZE, '\n');
	cin.ignore(100, '\n');
	head->data.flavor = new char [strlen(temp2) + 1];
	strcpy(head->data.flavor, temp2);

	do
	{
		cout << "\nWhat would you rate the food cart? (1-10): ";
		cin >> head->data.rate;
		cin.ignore(100, '\n');
		if (head->data.rate < 1 || head->data.rate > 10)
			cout << "\nNumber out of range, try again";
	} while (head->data.rate < 1 || head->data.rate > 10);

	head->next = hold;

}

void insert(node * & head)
{	
	int size {0};
	int count {0};
	cout << "\nHow many food cart items do you want to add?: ";
	cin >> size;
	cin.ignore(100, '\n');

	do
	{
		read(head);
		++count;
	} while (count < size && again());

}

bool again()
{
	char response {'y'};
	do
	{
		cout << "\nDo you want to add another? y/n: ";
		cin >> response;
		cin.ignore(100, '\n');
		if (tolower(response) != 'y' && tolower(response) != 'n')
			cout << "\nIncorrect response, try again";
	} while (tolower(response) != 'y' && tolower(response) != 'n');

	if (response == 'y')
		return true;
	return false;
}

void display(node * head)
{
	cout << "\n***FOOD CART LIST***" << endl;
	while (head)
	{
		cout << "\nName: " << head->data.name
			<< "\nFavorite Food: " << head->data.fave
			<< "\nCost: $" << head->data.cost
			<< "\nDescription: " << head->data.flavor
			<< "\nRating: " << head->data.rate << "/10" << endl;
		head = head->next;
	}
}

void destroy(node * & head)
{
	if (!head)
		return;
	node * current = head;
	while (head)
	{
		current = current->next;
		delete [] head->data.name;
		delete [] head->data.fave;
		delete [] head->data.flavor;
		delete head;
		head = current;
	}
}
